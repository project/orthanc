<?php

namespace Drupal\orthanc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Study entities.
 *
 * @ingroup orthanc
 */
class StudyEntityDeleteForm extends ContentEntityDeleteForm {


}
