<?php

namespace Drupal\orthanc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Modality entities.
 *
 * @ingroup orthanc
 */
class ModalityEntityDeleteForm extends ContentEntityDeleteForm {


}
