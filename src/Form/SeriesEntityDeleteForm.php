<?php

namespace Drupal\orthanc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Series entities.
 *
 * @ingroup orthanc
 */
class SeriesEntityDeleteForm extends ContentEntityDeleteForm {


}
