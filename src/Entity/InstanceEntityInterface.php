<?php

namespace Drupal\orthanc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Instance entities.
 *
 * @ingroup orthanc
 */
interface InstanceEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Instance name.
   *
   * @return string
   *   Name of the Instance.
   */
  public function getName();

  /**
   * Sets the Instance name.
   *
   * @param string $name
   *   The Instance name.
   *
   * @return \Drupal\orthanc\Entity\InstanceEntityInterface
   *   The called Instance entity.
   */
  public function setName($name);

  /**
   * Gets the Instance creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Instance.
   */
  public function getCreatedTime();

  /**
   * Sets the Instance creation timestamp.
   *
   * @param int $timestamp
   *   The Instance creation timestamp.
   *
   * @return \Drupal\orthanc\Entity\InstanceEntityInterface
   *   The called Instance entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Instance published status indicator.
   *
   * Unpublished Instance are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Instance is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Instance.
   *
   * @param bool $published
   *   TRUE to set this Instance to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\orthanc\Entity\InstanceEntityInterface
   *   The called Instance entity.
   */
  public function setPublished($published);

}
