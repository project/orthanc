<?php

namespace Drupal\orthanc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Patient entities.
 *
 * @ingroup orthanc
 */
class PatientEntityDeleteForm extends ContentEntityDeleteForm {


}
