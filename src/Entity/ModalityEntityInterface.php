<?php

namespace Drupal\orthanc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Modality entities.
 *
 * @ingroup orthanc
 */
interface ModalityEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Modality name.
   *
   * @return string
   *   Name of the Modality.
   */
  public function getName();

  /**
   * Sets the Modality name.
   *
   * @param string $name
   *   The Modality name.
   *
   * @return \Drupal\orthanc\Entity\ModalityEntityInterface
   *   The called Modality entity.
   */
  public function setName($name);

  /**
   * Gets the Modality creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Modality.
   */
  public function getCreatedTime();

  /**
   * Sets the Modality creation timestamp.
   *
   * @param int $timestamp
   *   The Modality creation timestamp.
   *
   * @return \Drupal\orthanc\Entity\ModalityEntityInterface
   *   The called Modality entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Modality published status indicator.
   *
   * Unpublished Modality are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Modality is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Modality.
   *
   * @param bool $published
   *   TRUE to set this Modality to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\orthanc\Entity\ModalityEntityInterface
   *   The called Modality entity.
   */
  public function setPublished($published);

}
