<?php

namespace Drupal\orthanc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Instance entities.
 *
 * @ingroup orthanc
 */
class InstanceEntityDeleteForm extends ContentEntityDeleteForm {


}
